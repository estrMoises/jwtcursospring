package com.bolsadeideas.springboot.app.auth.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import com.bolsadeideas.springboot.app.auth.SimpleGrantedAuthorityMixin;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTServiceImpl implements JWTService {
	
	public static final String SECRET = Base64Utils.encodeToString("algunaLlaveSecreta1algunaLlaveSecreta1".getBytes());
	public static final long EXPIRATE_DATE = 14000000L;
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";

	@Override
	public String createJWT(Authentication authResult) throws IOException {
		
		String username = ((User) authResult.getPrincipal()).getUsername();
		Collection<? extends GrantedAuthority> roles = authResult.getAuthorities();
		final SecretKey SECRET_KEY = new SecretKeySpec(SECRET.getBytes(), SignatureAlgorithm.HS256.getJcaName());
		
		Claims reclamaciones = Jwts.claims();
		reclamaciones.put("authorities", new ObjectMapper().writeValueAsString(roles));
        String token = Jwts.builder()
        		        .setClaims(reclamaciones)    		
                        .setSubject(username)
                        .signWith(SECRET_KEY)
                        .setIssuedAt(new Date())
                        .setExpiration(new Date(System.currentTimeMillis() + 14000000L))
                        .compact();
		return token;
	}

	@Override
	public boolean validate(String token) {
	
	boolean validToken = false;
		try {
			getClaims(token);
			validToken = true;
		} catch (JwtException e) {
			validToken = false;
		}
		return validToken;
	}

	@Override
	public Claims getClaims(String token) {
		Claims claims = Jwts
				.parserBuilder().setSigningKey(SECRET.getBytes())
			    .build()
			    .parseClaimsJws(resolve(token)).getBody();
		return claims;
	}

	@Override
	public String getUsername(String token) {
		return getClaims(token).getSubject();
	}

	@Override
	public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException {
		Object roles = getClaims(token).get("authorities");
		Collection<? extends GrantedAuthority> authorities = Arrays.asList(new ObjectMapper()
				                                                   .addMixIn(SimpleGrantedAuthority.class, SimpleGrantedAuthorityMixin.class)
				                                                   .readValue(roles.toString().getBytes(), SimpleGrantedAuthority[].class));
		return authorities;
	}

	@Override
	public String resolve(String token){
		if(token != null && token.startsWith(TOKEN_PREFIX)) 
		{
			return token.replace(TOKEN_PREFIX, "");
		}
		return null;
	}

}
